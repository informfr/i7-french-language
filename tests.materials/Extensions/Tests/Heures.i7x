Version 1/201008 of Heures by Tests begins here.

Part - Afficher l'heure numérique

Simple unit test:
	repeat through the table of heures numériques:
		assert "afficher l'heure numérique à [heure entry]" that "[heure entry]" exactly matches the text "[résultat entry]".

Table of heures numériques
heure	résultat
12:00 AM	"0[_]h"
12:09 AM	"0[_]h[_]9"
9:54 AM	"9[_]h[_]54"
12:00 PM	"12[_]h"
12:30 PM	"12[_]h[_]30"
6:45 PM	"18[_]h[_]45"

Part - Écrire l'heure en lettres

Simple unit test:
	repeat through the table of heures en lettres:
		assert "afficher l'heure en lettres à [heure entry]" that "[heure entry in words]" exactly matches the text "[résultat entry]".

Table of heures en lettres
heure	résultat
12:00 AM	"minuit"
12:09 AM	"minuit neuf"
1:00 AM	"une heure"
1:7 AM	"une heure sept"
1:15 AM	"une heure et quart"
1:27 AM	"une heure vingt-sept"
1:30 AM	"une heure et demie"
1:42 AM	"deux heures moins dix-huit"
1:45 AM	"deux heures moins le quart"
2:21 AM	"deux heures vingt et une"
9:54 AM	"dix heures moins six"
12:00 PM	"midi"
12:30 PM	"midi et demie"
6:45 PM	"dix-neuf heures moins le quart"
9:13 PM	"vingt et une heures treize"

Heures ends here.
