Version 1/201008 of Determinants by Tests begins here.

Part - Si un texte commence par une voyelle

[Pas forcément le meilleur endroit où mettre ce test, mais comme la phrase testée est surtout utilisée pour choisir les déterminant, ça ira.]

Simple unit test:
	assert "texte commençant par la voyelle a" that "azerty" starts with a vowel;
	assert "texte commençant par la voyelle A" that "Azerty" starts with a vowel;
	assert "texte commençant par la voyelle â" that "âzerty" starts with a vowel;
	assert "texte commençant par la voyelle Â" that "Âzerty" starts with a vowel;
	assert "texte commençant par la voyelle à" that "àzerty" starts with a vowel;
	assert "texte commençant par la voyelle À" that "Àzerty" starts with a vowel;
	assert "texte commençant par la voyelle e" that "ezerty" starts with a vowel;
	assert "texte commençant par la voyelle E" that "Ezerty" starts with a vowel;
	assert "texte commençant par la voyelle é" that "ézerty" starts with a vowel;
	assert "texte commençant par la voyelle É" that "Ézerty" starts with a vowel;
	assert "texte commençant par la voyelle è" that "èzerty" starts with a vowel;
	assert "texte commençant par la voyelle È" that "Èzerty" starts with a vowel;
	assert "texte commençant par la voyelle ê" that "êzerty" starts with a vowel;
	assert "texte commençant par la voyelle Ê" that "Êzerty" starts with a vowel;
	assert "texte commençant par la voyelle i" that "izerty" starts with a vowel;
	assert "texte commençant par la voyelle I" that "Izerty" starts with a vowel;
	assert "texte commençant par la voyelle î" that "îzerty" starts with a vowel;
	assert "texte commençant par la voyelle Î" that "Îzerty" starts with a vowel;
	assert "texte commençant par la voyelle o" that "ozerty" starts with a vowel;
	assert "texte commençant par la voyelle O" that "Ozerty" starts with a vowel;
	assert "texte commençant par la voyelle ô" that "ôzerty" starts with a vowel;
	assert "texte commençant par la voyelle Ô" that "Ôzerty" starts with a vowel;
	assert "texte commençant par la voyelle u" that "uzerty" starts with a vowel;
	assert "texte commençant par la voyelle U" that "Uzerty" starts with a vowel;
	assert "texte commençant par la voyelle û" that "ûzerty" starts with a vowel;
	assert "texte commençant par la voyelle Û" that "Ûzerty" starts with a vowel;
	assert "texte commençant par la voyelle ü" that "üzerty" starts with a vowel;
	assert "texte commençant par la voyelle Ü" that "Üzerty" starts with a vowel;
	assert "texte commençant par la voyelle æ" that "æzerty" starts with a vowel;
	assert "texte commençant par la voyelle Æ" that "Æzerty" starts with a vowel;
	assert "texte commençant par la voyelle œ" that "œzerty" starts with a vowel;
	assert "texte commençant par la voyelle Œ" that "Œzerty" starts with a vowel;
	assert "texte commençant par la voyelle h" that "hzerty" starts with a vowel;
	assert "texte commençant par la voyelle H" that "Hzerty" starts with a vowel;
	assert "texte ne commençant pas par une voyelle" that not ("qwerty" starts with a vowel);
	assert "texte ne commençant pas par une voyelle quand vide" that not ("" starts with a vowel);

Part - Le, la et les

Simple unit test:
	repeat through the table of le-la-les:
		assert "article défini avec [sujet entry]" that "[le sujet entry]" exactly matches the text "[résultat minuscule entry]";
		assert "article défini majuscule avec [sujet entry]" that "[Le sujet entry]" exactly matches the text "[résultat majuscule entry]";

Table of le-la-les
sujet	résultat minuscule	résultat majuscule
concept-MS	"le concept-MS"	"Le concept-MS"
aconcept-MS	"l'aconcept-MS"	"L'aconcept-MS"
concepts-MP	"les concepts-MP"	"Les concepts-MP"
aconcepts-MP	"les aconcepts-MP"	"Les aconcepts-MP"
notion-FS	"la notion-FS"	"La notion-FS"
anotion-FS	"l'anotion-FS"	"L'anotion-FS"
notions-FP	"les notions-FP"	"Les notions-FP"
anotions-FP	"les anotions-FP"	"Les anotions-FP"
Gustave	"Gustave"	"Gustave"
Albert	"Albert"	"Albert"
Céleste	"Céleste"	"Céleste"
Aoda	"Aoda"	"Aoda"

Part - Un, une, des

Simple unit test:
	repeat through the table of un-une-des:
		assert "article indéfini avec [sujet entry]" that "[un sujet entry]" exactly matches the text "[résultat minuscule entry]";
		assert "article indéfini majuscule avec [sujet entry]" that "[Un sujet entry]" exactly matches the text "[résultat majuscule entry]";

Table of un-une-des
sujet	résultat minuscule	résultat majuscule
concept-MS	"un concept-MS"	"Un concept-MS"
aconcept-MS	"un aconcept-MS"	"Un aconcept-MS"
concepts-MP	"des concepts-MP"	"Des concepts-MP"
aconcepts-MP	"des aconcepts-MP"	"Des aconcepts-MP"
notion-FS	"une notion-FS"	"Une notion-FS"
anotion-FS	"une anotion-FS"	"Une anotion-FS"
notions-FP	"des notions-FP"	"Des notions-FP"
anotions-FP	"des anotions-FP"	"Des anotions-FP"
Gustave	"Gustave"	"Gustave"
Albert	"Albert"	"Albert"
Céleste	"Céleste"	"Céleste"
Aoda	"Aoda"	"Aoda"

Part - La propriété Inform 6 articles

Simple unit test:
	[H aspiré.]
	assert "article défini pour un H aspiré" that "[le haricot]" exactly matches the text "le haricot";
	assert "article défini avec une majuscule pour un H aspiré" that "[Le haricot]" exactly matches the text "Le haricot";
	assert "article indéfini pour un H aspiré" that "[un haricot]" exactly matches the text "un haricot";
	assert "article indéfini avec une majuscule pour un H aspiré" that "[Un haricot]" exactly matches the text "Un haricot";
	[Œ.]
	assert "article défini pour un Œ" that "[l' oeuf]" exactly matches the text "l[']œuf";
	assert "article défini avec une majuscule pour un Œ" that "[L' oeuf]" exactly matches the text "L[']œuf";
	assert "article indéfini pour un Œ" that "[un oeuf]" exactly matches the text "un œuf";
	assert "article indéfini avec une majuscule pour un Œ" that "[Un oeuf]" exactly matches the text "Un œuf";
	[Article défini élidé en indefinite article.]
	assert "article défini pour un article défini élidé en indefinite article" that "[l' Europe]" exactly matches the text "l'Europe";
	assert "article défini avec une majuscule pour un article défini élidé en indefinite article" that "[L' Europe]" exactly matches the text "L'Europe";
	assert "article indéfini pour un article défini élidé en indefinite article" that "[une Europe]" exactly matches the text "l'Europe";
	assert "article indéfini avec une majuscule pour un article défini élidé en indefinite article" that "[Une Europe]" exactly matches the text "L'Europe".

Part - De, du et des

Simple unit test:
	repeat through the table of de-du-des:
		assert "préposition «[_]de[_]» avec [sujet entry]" that "[du sujet entry]" exactly matches the text "[résultat entry]";

Table of de-du-des
sujet	résultat
concept-MS	"du concept-MS"
aconcept-MS	"de l'aconcept-MS"
concepts-MP	"des concepts-MP"
aconcepts-MP	"des aconcepts-MP"
notion-FS	"de la notion-FS"
anotion-FS	"de l'anotion-FS"
notions-FP	"des notions-FP"
anotions-FP	"des anotions-FP"
Gustave	"de Gustave"
Albert	"d'Albert"
Céleste	"de Céleste"
Aoda	"d'Aoda"

Part - À, au et aux

Simple unit test:
	repeat through the table of à-au-aux:
		assert "préposition « à » avec [sujet entry]" that "[à sujet entry]" exactly matches the text "[résultat entry]";

Table of à-au-aux
sujet	résultat
concept-MS	"au concept-MS"
aconcept-MS	"à l'aconcept-MS"
concepts-MP	"aux concepts-MP"
aconcepts-MP	"aux aconcepts-MP"
notion-FS	"à la notion-FS"
anotion-FS	"à l'anotion-FS"
notions-FP	"aux notions-FP"
anotions-FP	"aux anotions-FP"
Gustave	"à Gustave"
Albert	"à Albert"
Céleste	"à Céleste"
Aoda	"à Aoda"

Determinants ends here.
