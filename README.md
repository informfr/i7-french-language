# Instructions #

Ceci est l'extension permettant de créer des fictions interactives avec Inform 7 6L38. NE FONcTIONNE PAS ENCORE AVEC 6M62 !

## Installer l'extension ##

* Télécharger la dernière version de l'extension *et* le fichier « about.txt » [ici](https://bitbucket.org/informfr/i7-french-language/src)
* Déplacer le fichier « about.txt » dans \Inform7\Extensions\Reserved\Languages\French\about.txt (sur Mac, cliquer droit sur l'icône de l'application Inform 7 -> afficher le contenu du paquet)
* Pour écrire un jeu en français, écrire « (in French) » après le titre du jeu. **Ne pas écrire la ligne « Include French Language by Nathanael Marion » !**
* Voir l'exemple ci-dessous.

```
#!Inform

"Mon jeu à moi" by Natrium729 (in French)

La cuisine est un endroit.
```

## Fonctionnalités expérimentales ##

Toutes les fonctionnalités expérimentales sont regroupées dans l'extension **Experimental French Features.i7x**. Elle permet d'écrire une grande partie de son code source en français (syntaxe, adjectifs et variables). Vous pouvez l'utiliser si vous le souhaitez, mais il se peut qu'elle ne fonctionne pas parfaitement, alors attention !

## Nous aider ##

Pour aider, il suffit d'écrire sa fiction interactive avec la nouvelle version d'Inform, et de signaler les bugs.