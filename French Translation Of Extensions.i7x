Version 1/160103 of French Translation Of Extensions by Nathanael Marion begins here.

"Translates into French the extensions that are not built-in. Contact the author if you want an extension you use to be translated.".

Volume 1 - Rideable Vehicles (for use with Rideable Vehicles by Graham Nelson)

[Because we cannot translate kinds in a language extension.]

A rideable animal translates into French as un animal chevauchable. The plural of animal chevauchable is animaux chevauchables.
A rideable vehicle translates into French as un véhicule chevauchable. The plural of véhicule chevauchable is véhicules chevauchables.

Volume 2 - Flexible Windows by Jon Ingold (for use with Flexible Windows by Jon Ingold)

Book 1 - Kinds

A g-window translates into French as une g-fenêtre.

A g-window type translates into French as un type de g-fenêtre. The plural of type de g-fenêtre is types de g-fenêtre.
g-text-buffer translates into French as g-tampon-de-texte. The plural of g-tampon-de-texte is g-tampons-de-texte.
g-text-grid translates into French as g-grille-de-texte. The plural of g-grille-de-texte is g-grilles-de-texte.
g-graphics translates into French as g-graphismes. The plural of g-graphismes is g-graphismes.

A g-window position translates into French as une position de g-fenêtre. The plural of position de g-fenêtre is positions de g-fenêtre.
g-placenull translates into French as g-nullepart. The plural of g-nullepart is types de g-nullepart.
g-placeleft translates into French as g-àgauche. The plural of g-àgauche is g-àgauche.
g-placeright translates into French as g-àdroite. The plural of g-droite is g-àdroite.
g-placeabove translates into French as un g-enhaut. The plural of g-enhaut is g-enhaut.
g-placebelow translates into French as g-enbas. The plural of g-enbas is g-enbas.

[TODO
A graphics g-window is a kind of g-window.
A text buffer g-window is a kind of g-window.
A text grid g-window is a kind of g-window.

A g-window scale method is a kind of value.
The g-window scale methods are g-proportional, g-fixed-size and g-using-minimum.
]

Book 2 - Things

The main window translates into French as la fenêtre principale.
The status window translates into French as la fenêtre de statut.

Book 3 - Verbs

In French engendrer is a verb meaning to spawn.
In French être un parent de is a verb meaning to be ancestral to. [TODO]
In French descendre de is a verb meaning to be descended from.

Book 4 - Specifications

The specification of a g-window is "Modélise le système de fenêtre Glk.".
The specification of a g-window type is "Les fenêtres Glk ont l'un des ces trois types.".
The specification of a g-window position is "Spécifie dans quelle direction une fenêtre va être séparée de son parent.".
The specification of a g-window scale method is "Spécifie comment une nouvelle fenêtre sera découpée de son parent.".

Book 5 - Phrases and definitions (for use with Experimental French Features by Nathanael Marion)

To ouvrir (win - a g-window), en tant que fenêtre principale:
	if en tant que fenêtre principale:
		open win, as the acting main window;
	else:
		open win.

To fermer (win - a g-window):
	close win.

To effacer (win - a g-window):
	clear win.

To actualiser (win - a g-window):
	safely carry out the refreshing activity with win;

To actualiser toutes les fenêtres:
	refresh all windows.

To decide what number is the hauteur du/de/des (win - a g-window):
	(- FW_WindowSize( {win}, 1 ) -).

To decide what number is the largeur du/de/des (win - a g-window):
	(- FW_WindowSize( {win}, 0 ) -).

Definition: a g-window is graphique rather than textuelle if the type of it is g-graphics.
Definition: a g-window is positionnée verticalement rather than positionnée horizontalement if the position of it is at least g-placeabove.

Volume 3 - Glulx Real Time by Erik Temple (for use with Glulx Real Time by John Ingold)

Book 1 - Kinds

A virtual timer translates into French as un minuteur vituel. The plural of minuteur virtuel is minuteurs virtuels

Book 2 - Phrases and definitions (for use with Experimental French Features by Nathanael Marion)

To pauser les minuteurs virtuels: [TODO: virtualized -> virtuels ?]
	pause virtualized timers.

To reprendre les minuteurs virtuels:
	restart virtualized timers.

French Translation Of Extensions ends here.


























